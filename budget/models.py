from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.

class Budget(models.Model):
    name = models.CharField(max_length=120)
    description = models.TextField(null=True, blank=True)
    estimate = models.PositiveSmallIntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(100000)])
    actual = models.PositiveSmallIntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(100000)])

    def __str__(self):
        return self.name