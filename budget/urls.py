from django.urls import path

from budget.views import (
    BudgetCreateView,
    BudgetDeleteView,
    BudgetDetailView,
    BudgetListView,
    BudgetUpdateView,
)

urlpatterns = [
    path("", BudgetListView.as_view(), name="budget_list"),
    path("<int:pk>/delete/", BudgetDeleteView.as_view(), name="delete_budget"),
    path("create/", BudgetCreateView.as_view(), name="create_budget"),
    path("<int:pk>/complete/", BudgetUpdateView.as_view(), name="update_budget"),
    path("<int:pk>/", BudgetDetailView.as_view(), name="budget_detail")
    
]