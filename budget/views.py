from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin

from budget.models import Budget

# Create your views here.

class BudgetListView(LoginRequiredMixin, ListView):
    model = Budget
    template_name = "budget/list.html"

class BudgetCreateView(LoginRequiredMixin, CreateView):
    model = Budget
    template_name = "budget/create.html"
    fields = ["name", "description", "estimate", "actual"]

    def form_valid(self, form):
        budget = form.save(commit=False)
        budget.save()
        form.save()
        return redirect("budget_list")

class BudgetDeleteView(LoginRequiredMixin, DeleteView):
    model = Budget
    template_name = "budget/delete.html"
    success_url = reverse_lazy("budget_list")

class BudgetDetailView(LoginRequiredMixin, DetailView):
    model = Budget
    template_name = "budget/detail.html"

class BudgetUpdateView(LoginRequiredMixin, UpdateView):
    model = Budget
    template_name = "budget/update.html"
    fields = ["name", "description", "estimate", "actual"]

    def form_valid(self, form):
        budget = form.save(commit=False)
        budget.save()
        form.save()
        return redirect("budget_list")