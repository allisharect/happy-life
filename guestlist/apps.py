from django.apps import AppConfig


class GuestlistConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'guestlist'
