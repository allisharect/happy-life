from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.

class GuestList(models.Model):
    name = models.CharField(max_length=200)
    plus = models.PositiveSmallIntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(5)])
    rsvp = models.BooleanField(default=False)
    
    def __str__(self):
        return self.name