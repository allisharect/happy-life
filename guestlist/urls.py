from django.urls import path

from guestlist.views import (
    GuestListDeleteView,
    GuestListCreateView,
    GuestListListView,
    GuestListUpdateView
)

urlpatterns = [
    path("", GuestListListView.as_view(), name="guest_list"),
    path("<int:pk>/delete/", GuestListDeleteView.as_view(), name="delete_guest"),
    path("create/", GuestListCreateView.as_view(), name="create_guest"),
    path("<int:pk>/complete/", GuestListUpdateView.as_view(), name="complete_rsvp")
    
]