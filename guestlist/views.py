from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from guestlist.models import GuestList
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.

class GuestListListView(LoginRequiredMixin, ListView):
    model = GuestList
    template_name = "guestlist/list.html"

class GuestListDeleteView(LoginRequiredMixin, DeleteView):
    model = GuestList
    template_name = "guestlist/delete.html"
    success_url = reverse_lazy("guest_list")

class GuestListCreateView(LoginRequiredMixin, CreateView):
    model = GuestList
    template_name = "guestlist/create.html"
    fields = ["name", "plus", "rsvp"]

    def form_valid(self, form):
        guestlist = form.save(commit=False)
        guestlist.save()
        form.save()
        return redirect("guest_list")

class GuestListUpdateView(LoginRequiredMixin, UpdateView):
    model = GuestList
    template_name = "guestlist/list.html"
    fields = ["rsvp"]
    success_url = reverse_lazy("guest_list")

    def form_valid(self, form):
        guestlist = form.save(commit=False)
        guestlist.save()
        form.save()
        return redirect("guest_list")
