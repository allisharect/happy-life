from django.contrib import admin
from weddings.models import Wedding
from guestlist.models import GuestList

# Register your models here.

class WeddingAdmin(admin.ModelAdmin):
    pass

class GuestListAdmin(admin.ModelAdmin):
    pass

admin.site.register(Wedding, WeddingAdmin)
admin.site.register(GuestList, GuestListAdmin)