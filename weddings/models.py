from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.

class Wedding(models.Model):
    name = models.CharField(max_length=300)
    date = models.CharField(max_length=10)
    guests = models.PositiveSmallIntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(5000)])
    budget = models.PositiveSmallIntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(100000)])

    def __str__(self):
        return self.name