from django.urls import path

from weddings.views import (
    WeddingCreateView,
    WeddingDeleteView,
    WeddingUpdateView,
    WeddingDetailView,
    WeddingListView,
)

urlpatterns = [
    path("", WeddingListView.as_view(), name="wedding_list"),
    path("<int:pk>/delete/", WeddingDeleteView.as_view(), name="delete_wedding"),
    path("<int:pk>/edit/", WeddingUpdateView.as_view(), name="update_wedding"),
    path("<int:pk>/", WeddingDetailView.as_view(), name="wedding_detail"),
    path("create/", WeddingCreateView.as_view(), name="create_wedding"),
]