from ast import Delete
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin

from weddings.models import Wedding

# Create your views here.

class WeddingListView(LoginRequiredMixin, ListView):
    model = Wedding
    template_name = "weddings/list.html"


class WeddingDetailView(LoginRequiredMixin, DetailView):
    model = Wedding
    template_name = "weddings/detail.html"

class WeddingUpdateView(LoginRequiredMixin, UpdateView):
    model = Wedding
    template_name = "weddings/update.html"
    fields = ["name", "date", "guests", "budget"]
    success_url = reverse_lazy("wedding_list")

class WeddingCreateView(LoginRequiredMixin, CreateView):
    model = Wedding
    template_name = "weddings/create.html"
    fields = ["name", "date", "guests", "budget"]

    def form_valid(self, form):
        wedding = form.save(commit=False)
        wedding.save()
        form.save()
        return redirect("wedding_detail", pk=wedding.id)


class WeddingDeleteView(LoginRequiredMixin, DeleteView):
    model = Wedding
    template_name = "weddings/delete.html"
    success_url = reverse_lazy("wedding_list")
